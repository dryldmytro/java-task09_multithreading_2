package com.dryl;

import java.util.ResourceBundle;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Constant {

  public static Scanner scanner = new Scanner(System.in);
  public static Logger logger = LogManager.getLogger(Constant.class);
  public static ResourceBundle bundle = ResourceBundle.getBundle("constants");
  public static int SLEEP1 = 1000;
  public static int SLEEP2 = 7000;
  public static int ZERO = 0;

  public static void main(String[] args) {
    logger.info(bundle.getString("name1"));
  }

}
