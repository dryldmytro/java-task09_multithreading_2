package com.dryl;

import com.dryl.view.View;

public class Main {

  public static void main(String[] args) {
    View view = new View();
    view.outputMenu();
  }

}
