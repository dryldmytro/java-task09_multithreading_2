package com.dryl.view;

import static com.dryl.Constant.bundle;
import static com.dryl.Constant.logger;
import static com.dryl.Constant.scanner;

import com.dryl.controller.Controller;
import java.util.LinkedHashMap;
import java.util.Map;

public class View {

  private Controller controller;
  Map<String, String> map;

  public View() {
    controller = new Controller();
    map = new LinkedHashMap<>();
    map.put("1", bundle.getString("task1"));
    map.put("2", bundle.getString("task2"));
    map.put("3", bundle.getString("task3"));
    map.put("Q", bundle.getString("Q"));
  }

  public void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : map.values()) {
      logger.trace(str);
    }
    logger.trace(bundle.getString("choice"));
    String userChoice = scanner.nextLine().toUpperCase();
    switch (userChoice) {
      case "1":
        controller.getTask1();
        outputMenu();
      case "2":
        controller.getTask2();
        outputMenu();
      case "3":
        controller.getTask3();
        outputMenu();
      case "Q":
        System.exit(0);
      default:
        logger.trace(bundle.getString("bad_choice"));
        outputMenu();
    }
  }
}
