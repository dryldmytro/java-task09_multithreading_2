package com.dryl.model.task3;

public interface MyLock {
  void lock();
  void unlock();
  boolean tryLock();

}
