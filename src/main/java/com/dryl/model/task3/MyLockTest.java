package com.dryl.model.task3;

import static com.dryl.Constant.SLEEP1;
import static com.dryl.Constant.SLEEP2;
import static com.dryl.Constant.bundle;
import static com.dryl.Constant.logger;

public class MyLockTest {
  MyLockImpl m = new MyLockImpl();
  public void show(){
    Thread thread1=new Thread1();
    Thread thread2=new Thread1();
    Thread thread3=new Thread1();
    Thread thread4=new Thread2();
    thread1.start();
    thread2.start();
    thread3.start();
    thread4.start();
    try {
      thread1.join();
      thread2.join();
      thread3.join();
      thread4.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  private class Thread1 extends Thread{
    @Override
    public void run() {
      m.lock();
      try {
        logger.info(getName()+bundle.getString("start"));
        sleep(SLEEP1);
        logger.info(getName()+bundle.getString("end"));
        sleep(SLEEP1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      m.unlock();
    }
  }
  private class Thread2 extends Thread{

    @Override
    public void run() {
      try {
        sleep(SLEEP1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (m.tryLock()){
        try {
          logger.info(getName()+bundle.getString("start"));
          sleep(SLEEP1);
          logger.info(getName()+bundle.getString("end"));
          sleep(SLEEP1);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }else {
        logger.warn(getName()+bundle.getString("wait"));
      }
    }
  }
}
