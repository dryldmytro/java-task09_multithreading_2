package com.dryl.model.task3;

import static com.dryl.Constant.ZERO;

public class MyLockImpl implements MyLock {
  private int lockHoldCount;

  public MyLockImpl() {
    lockHoldCount = ZERO;
  }

  @Override
  public synchronized void lock() {
    if (lockHoldCount==ZERO){
      lockHoldCount++;
    } else{
      try {
        wait();
        lockHoldCount++;
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public synchronized void unlock() {
    lockHoldCount--;
    if (lockHoldCount==ZERO){
      notify();
    }
  }

  @Override
  public synchronized boolean tryLock() {
    if (lockHoldCount==ZERO){
      lock();
      return true;
    }else return false;
  }
}
