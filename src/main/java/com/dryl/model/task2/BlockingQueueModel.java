package com.dryl.model.task2;

import static com.dryl.Constant.SLEEP1;
import static com.dryl.Constant.bundle;
import static com.dryl.Constant.logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueModel {
  private BlockingQueue queue;
  public String text = bundle.getString("sometext");
  public BlockingQueueModel() {
    int i = text.length();
    queue = new ArrayBlockingQueue(i);
  }

  public void pipeCommunication(){
    Thread thread1=new Thread2();
    Thread thread2=new Thread1();
    thread1.start();
    thread2.start();
    try {
      thread1.join();
      thread2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }
  private class Thread1 extends Thread{

    @Override
    public void run() {
      try {
        while (!queue.isEmpty()){
          logger.info(queue.take());
          sleep(SLEEP1);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  private class Thread2 extends Thread{

    @Override
    public void run() {
      for (int i = 0; i < text.length(); i++) {
        queue.add(text.charAt(i));
        try {
          sleep(SLEEP1);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
