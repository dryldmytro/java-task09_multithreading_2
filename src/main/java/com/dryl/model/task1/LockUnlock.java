package com.dryl.model.task1;

import static com.dryl.Constant.SLEEP1;
import static com.dryl.Constant.bundle;
import static com.dryl.Constant.logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockUnlock {
  private Lock lock = new ReentrantLock();

  public void show() {
    Thread thread1 = new ThreadWithLock();
    Thread thread2=new ThreadWithLock();
    Thread thread3=new ThreadWithLock();
    thread1.start();
    thread2.start();
    thread3.start();
    try {
      thread1.join();
      thread2.join();
      thread3.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }
  private class ThreadWithLock extends Thread{

    @Override
    public void run() {
      lock.lock();
      try {
        logger.info(getName()+bundle.getString("start"));
        sleep(SLEEP1);
        logger.info(getName()+bundle.getString("end"));
        sleep(SLEEP1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      finally {
        lock.unlock();
      }
    }
  }
}
