package com.dryl.controller;

import com.dryl.model.task1.LockUnlock;
import com.dryl.model.task2.BlockingQueueModel;
import com.dryl.model.task3.MyLockTest;

public class Controller {
  public void getTask1(){
    LockUnlock l = new LockUnlock();
    l.show();
  }
  public void getTask2(){
    BlockingQueueModel b = new BlockingQueueModel();
    b.pipeCommunication();
  }
  public void getTask3(){
    MyLockTest m =  new MyLockTest();
    m.show();
  }
}
